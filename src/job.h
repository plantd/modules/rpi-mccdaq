#ifndef APEX_RPI_MCCDAQ_JOB_H
#define APEX_RPI_MCCDAQ_JOB_H

#include <apex/apex.h>
#include <glib.h>

#include "device.h"

G_BEGIN_DECLS

#define APEX_TYPE_RPI_MCCDAQ_JOB apex_rpi_mccdaq_job_get_type ()
G_DECLARE_FINAL_TYPE (ApexRpiMccdaqJob, apex_rpi_mccdaq_job, APEX, RPI_MCCDAQ_JOB, ApexJob)

ApexJob             *apex_rpi_mccdaq_job_new     (ApexRpiMccdaqDevice *daq,
                                                  gchar               *job_name,
                                                  gchar               *job_value);

ApexRpiMccdaqDevice *apex_rpi_mccdaq_job_get_daq (ApexRpiMccdaqJob    *self);
void                 apex_rpi_mccdaq_job_set_daq (ApexRpiMccdaqJob    *self,
                                                  ApexRpiMccdaqDevice *daq);

G_END_DECLS

#endif /* APEX_RPI_MCCDAQ_JOB_H */
