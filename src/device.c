#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_HARDWARE
// TODO: include hardware dependencies
#endif

#include "device.h"

struct _ApexRpiMccdaqDevice
{
  GObject   parent;

  gboolean  connected;
  gboolean  acquiring;
  gchar    *file;
};

enum {
  PROP_0,
  PROP_ACQUIRING,
  PROP_CONNECTED,
  PROP_FILE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_TYPE (ApexRpiMccdaqDevice, apex_rpi_mccdaq_device, G_TYPE_OBJECT)

static void
apex_rpi_mccdaq_device_finalize (GObject *object)
{
  ApexRpiMccdaqDevice *self = (ApexRpiMccdaqDevice *) object;

  g_clear_pointer (&self->file, g_free);

  G_OBJECT_CLASS (apex_rpi_mccdaq_device_parent_class)->finalize (object);
}

static void
apex_rpi_mccdaq_device_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  ApexRpiMccdaqDevice *self = APEX_RPI_MCCDAQ_DEVICE (object);

  switch (prop_id)
    {
    case PROP_ACQUIRING:
      g_value_set_boolean (value, self->acquiring);
      break;

    case PROP_CONNECTED:
      g_value_set_boolean (value, self->connected);
      break;

    case PROP_FILE:
      g_value_set_string (value, self->file);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_rpi_mccdaq_device_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  ApexRpiMccdaqDevice *self = APEX_RPI_MCCDAQ_DEVICE (object);

  switch (prop_id)
    {
    case PROP_FILE:
      apex_rpi_mccdaq_device_set_file (self, g_value_dup_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
apex_rpi_mccdaq_device_class_init (ApexRpiMccdaqDeviceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = apex_rpi_mccdaq_device_finalize;
  object_class->get_property = apex_rpi_mccdaq_device_get_property;
  object_class->set_property = apex_rpi_mccdaq_device_set_property;

  properties [PROP_ACQUIRING] =
    g_param_spec_boolean ("acquiring",
                          "Acquiring",
                          "The device acquisition state",
                          FALSE,
                          (G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_CONNECTED] =
    g_param_spec_boolean ("connected",
                          "Connected",
                          "The device connection state",
                          FALSE,
                          (G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_FILE] =
    g_param_spec_string ("file",
                         "File",
                         "The device file to use",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
apex_rpi_mccdaq_device_init (ApexRpiMccdaqDevice *self)
{
}

ApexRpiMccdaqDevice *
apex_rpi_mccdaq_device_new (const gchar *file)
{
  g_return_val_if_fail (file != NULL, NULL);

  return g_object_new (APEX_TYPE_RPI_MCCDAQ_DEVICE,
                       "file", file,
                       NULL);
}

void
apex_rpi_mccdaq_device_connect (ApexRpiMccdaqDevice *self)
{
  g_return_if_fail (APEX_IS_RPI_MCCDAQ_DEVICE (self));

#ifdef HAVE_HARDWARE
  // TODO: handle the hardware
#endif

  self->connected = TRUE;
}

void
apex_rpi_mccdaq_device_disconnect (ApexRpiMccdaqDevice *self)
{
  g_return_if_fail (APEX_IS_RPI_MCCDAQ_DEVICE (self));

#ifdef HAVE_HARDWARE
  // TODO: handle the hardware
#endif

  self->connected = FALSE;
}

void
apex_rpi_mccdaq_device_reset (ApexRpiMccdaqDevice *self)
{
  ApexRpiMccdaqDevice *device;

  g_return_if_fail (APEX_IS_RPI_MCCDAQ_DEVICE (self));

  device = APEX_RPI_MCCDAQ_DEVICE (self);

  apex_rpi_mccdaq_device_stop_acquisition (device);
  apex_rpi_mccdaq_device_disconnect (device);
  apex_rpi_mccdaq_device_connect (device);
  apex_rpi_mccdaq_device_start_acquisition (device);
}

void
apex_rpi_mccdaq_device_start_acquisition (ApexRpiMccdaqDevice *self)
{
  g_return_if_fail (APEX_IS_RPI_MCCDAQ_DEVICE (self));

#ifdef HAVE_HARDWARE
  // TODO: handle the hardware
#endif

  self->acquiring = TRUE;
}

void
apex_rpi_mccdaq_device_stop_acquisition (ApexRpiMccdaqDevice *self)
{
  g_return_if_fail (APEX_IS_RPI_MCCDAQ_DEVICE (self));

#ifdef HAVE_HARDWARE
  // TODO: handle the hardware
#endif

  self->acquiring = FALSE;
}

gboolean
apex_rpi_mccdaq_device_is_connected (ApexRpiMccdaqDevice *self)
{
  g_return_val_if_fail (APEX_IS_RPI_MCCDAQ_DEVICE (self), FALSE);

  return self->connected;
}

gboolean
apex_rpi_mccdaq_device_is_acquiring (ApexRpiMccdaqDevice *self)
{
  g_return_val_if_fail (APEX_IS_RPI_MCCDAQ_DEVICE (self), FALSE);

  return self->acquiring;
}

const gchar *
apex_rpi_mccdaq_device_get_file (ApexRpiMccdaqDevice *self)
{
  g_return_val_if_fail (APEX_IS_RPI_MCCDAQ_DEVICE (self), NULL);

  return self->file;
}

void apex_rpi_mccdaq_device_set_file (ApexRpiMccdaqDevice *self,
                                      const gchar         *file)
{
  g_return_if_fail (APEX_IS_RPI_MCCDAQ_DEVICE (self));

  if (g_strcmp0 (file, self->file) != 0)
    {
      g_free (self->file);
      self->file = g_strdup (file);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_FILE]);
    }
}
